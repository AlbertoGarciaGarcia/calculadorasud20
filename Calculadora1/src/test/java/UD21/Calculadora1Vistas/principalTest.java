package UD21.Calculadora1Vistas;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class principalTest {

	@Test
	public void testSuma() {
		double resultado = principal.suma(2, 3, principal.operacion, principal.numero, principal.resultado, principal.historial);
		double esperado = 5;
		double delta = 1;
		assertEquals(resultado,esperado,delta);
	}
    public void testResta() {
  		double resultado = principal.suma(5, 2, principal.operacion, principal.numero, principal.resultado, principal.historial);
  		double esperado = 3;
  		double delta = 1;
  		assertEquals(resultado,esperado,delta);
  	}

}
