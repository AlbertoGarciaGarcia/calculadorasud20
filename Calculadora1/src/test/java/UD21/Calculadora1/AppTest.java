package UD21.Calculadora1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import UD21.Calculadora1Vistas.principal;
import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase
{
  
    @Test
   
    public void testsuma() {
    	principal calculadora = new principal();
		double esperado = 4;
		double delta = 1;
		principal.numero.setText("2");
		principal.operacion = 0;
		assertEquals(principal.suma(2.0, 3.0, principal.operacion,principal.numero, principal.resultado, principal.historial),esperado,delta);
		
	}
    public void testResta() {
    	principal calculadora = new principal();
		double esperado = 2;
		double delta = 1;
		principal.numero.setText("3");
		principal.operacion = 0;
		assertEquals(principal.resta(5.0, 3.0, principal.operacion,principal.numero, principal.resultado, principal.historial),esperado,delta);
  	}
    
    public void testMultiplicacion() {
    	principal calculadora = new principal();
		double esperado = 15;
		double delta = 1;
		principal.numero.setText("3");
		principal.operacion = 0;
		assertEquals(principal.multiplicacion(5.0, 3.0, principal.operacion,principal.numero, principal.resultado, principal.historial),esperado,delta);
  	}
    public void testDivision() {
    	principal calculadora = new principal();
		double esperado = 2;
		double delta = 1;
		principal.numero.setText("3");
		principal.operacion = 0;
		assertEquals(principal.division(6.0, 3.0, principal.operacion,principal.numero, principal.resultado, principal.historial),esperado,delta);
  	}
    public void testPorcentaje() {
    	principal calculadora = new principal();
		double esperado = 10;
		double delta = 1;
		principal.numero.setText("10");
		principal.operacion = 0;
		assertEquals(principal.porcentaje(100.0, 10.0, principal.operacion,principal.numero, principal.resultado, principal.historial),esperado,delta);
  	}
    public void testraizCuadrada() {
    	principal calculadora = new principal();
		double esperado = 3;
		double delta = 1;
		principal.numero.setText("9");
		principal.operacion = 0;
		assertEquals(principal.raizCuadrada(1,1, principal.operacion,principal.numero, principal.resultado, principal.historial),esperado,delta);
  	}
    
    public void testPotencia() {
    	principal calculadora = new principal();
		double esperado = 4;
		double delta = 1;
		principal.numero.setText("2");
		principal.operacion = 0;
		assertEquals(principal.potencia(1,1, principal.operacion,principal.numero, principal.resultado, principal.historial),esperado,delta);
  	}
    public void testdividir1() {
    	principal calculadora = new principal();
		double esperado = 0.1;
		double delta = 1;
		principal.numero.setText("10");
		principal.operacion = 0;
		assertEquals(principal.dividir1(1,1, principal.operacion,principal.numero, principal.resultado, principal.historial),esperado,delta);
  	}
	
}
