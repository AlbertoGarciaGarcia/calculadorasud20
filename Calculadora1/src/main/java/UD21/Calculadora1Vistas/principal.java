package UD21.Calculadora1Vistas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class principal extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	public static JLabel numero = new JLabel(" ");
	public static  JLabel resultado  = new JLabel(" ");
	public static double operacion;
	public static double n1;
	public static double n2;
	public static String ultimoCalculo;
	public static JTextArea historial;
	
	 
	public principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 559, 543);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		numero = new JLabel();
		numero.setBounds(10, 11, 94, 29);
		contentPane.add(numero);

		 historial = new JTextArea();
		historial.setBounds(388, 71, 145, 422);
		contentPane.add(historial);
		resultado.setBounds(20, 51, 46, 14);
		contentPane.add(resultado);
		JLabel lblNewLabel = new JLabel("Historial");
		lblNewLabel.setBounds(450, 11, 83, 14);
		contentPane.add(lblNewLabel);
	
		JButton btnNewButton = new JButton("1");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"1");
			}
		});
		btnNewButton.setBounds(0, 354, 94, 73);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("2");
		btnNewButton_1.setBounds(92, 354, 94, 73);
		contentPane.add(btnNewButton_1);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"2");
			}
		});
		
		JButton btnNewButton_1_1 = new JButton("3");
		btnNewButton_1_1.setBounds(185, 354, 94, 73);
		contentPane.add(btnNewButton_1_1);
		btnNewButton_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"3");
			}
		});
		
		JButton btnNewButton_1_2 = new JButton("+");
		btnNewButton_1_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ultimoCalculo = "+";
				n1 =  Double.parseDouble(numero.getText());
				resultado.setText(numero.getText() + "+");
				numero.setText(" ");
			}
		});
		btnNewButton_1_2.setBounds(278, 354, 94, 73);
		contentPane.add(btnNewButton_1_2);
		
		JButton btnNewButton_2 = new JButton("4");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"4");
			}
		});
		btnNewButton_2.setBounds(0, 282, 94, 73);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("7");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"7");
			}
		});
		btnNewButton_3.setBounds(0, 210, 94, 73);
		contentPane.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("1/X");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dividir1(n1,n2,operacion,numero,resultado,historial);
			}
		});
		btnNewButton_4.setBounds(0, 139, 94, 73);
		contentPane.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("%");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				porcentaje(n1,n2,operacion,numero,resultado,historial);
			}
		});
		btnNewButton_5.setBounds(0, 71, 94, 73);
		contentPane.add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("5");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"5");
			}
		});
		btnNewButton_6.setBounds(92, 282, 94, 73);
		contentPane.add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("8");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"8");
			}
		});
		btnNewButton_7.setBounds(92, 210, 94, 73);
		contentPane.add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("X^2");
		btnNewButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				potencia(n1,n2,operacion,numero,resultado,historial);
			
			}
		});
		btnNewButton_8.setBounds(92, 139, 94, 73);
		contentPane.add(btnNewButton_8);
		
		JButton btnNewButton_9 = new JButton("CE");
		btnNewButton_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(" ");
				n1 = 0;
			}
		});
		btnNewButton_9.setBounds(92, 71, 94, 73);
		contentPane.add(btnNewButton_9);
		
		JButton btnNewButton_10 = new JButton("6");
		btnNewButton_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"6");
			}
		});
		btnNewButton_10.setBounds(185, 282, 94, 73);
		contentPane.add(btnNewButton_10);
		
		JButton btnNewButton_11 = new JButton("9");
		btnNewButton_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"9");
			}
		});
		btnNewButton_11.setBounds(185, 210, 94, 73);
		contentPane.add(btnNewButton_11);
		
		JButton btnNewButton_12 = new JButton("√");
		btnNewButton_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				raizCuadrada(n1,n2,operacion,numero,resultado,historial);
	
			}
		});
		btnNewButton_12.setBounds(185, 139, 94, 73);
		contentPane.add(btnNewButton_12);
		
		JButton btnNewButton_13 = new JButton("C");
		btnNewButton_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(" ");
				resultado.setText(" ");
				n1 = 0;
				n2 = 0;
			}
		});
		btnNewButton_13.setBounds(185, 71, 94, 73);
		contentPane.add(btnNewButton_13);
		
		JButton btnNewButton_14 = new JButton("-");
		btnNewButton_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				ultimoCalculo = "-";
				n1 =  Double.parseDouble(numero.getText());
				resultado.setText(numero.getText() + "-");
				numero.setText(" ");
			}
		});
		btnNewButton_14.setBounds(278, 282, 94, 73);
		contentPane.add(btnNewButton_14);
		
		JButton btnNewButton_15 = new JButton("*");
		btnNewButton_15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ultimoCalculo = "*";
				n1 =  Double.parseDouble(numero.getText());
				resultado.setText(numero.getText() + "*");
				numero.setText(" ");
			}
			
		});
		btnNewButton_15.setBounds(278, 210, 94, 73);
		contentPane.add(btnNewButton_15);
		
		JButton btnNewButton_16 = new JButton("/");
		btnNewButton_16.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ultimoCalculo = "/";
				n1 =  Double.parseDouble(numero.getText());
				resultado.setText(numero.getText() + "/");
				numero.setText(" ");
			}
		});
		btnNewButton_16.setBounds(278, 139, 94, 73);
		contentPane.add(btnNewButton_16);
		
		JButton btnNewButton_17 = new JButton("Eliminar");
		btnNewButton_17.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(""+numero.getText().substring(0, numero.getText().length() - 1));
			}
		});
		btnNewButton_17.setBounds(278, 71, 94, 73);
		contentPane.add(btnNewButton_17);
		
	
		
		JButton btnNewButton_1_3 = new JButton("0");
		btnNewButton_1_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+"0");
			}
		});
		btnNewButton_1_3.setBounds(0, 420, 186, 73);
		contentPane.add(btnNewButton_1_3);
		
		JButton btnNewButton_1_4 = new JButton(",");
		btnNewButton_1_4.setBounds(185, 420, 94, 73);
		contentPane.add(btnNewButton_1_4);
		btnNewButton_1_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero.setText(numero.getText()+",");
			}
		});
		
		JButton btnNewButton_1_5 = new JButton("=");
		btnNewButton_1_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
	
				if(ultimoCalculo.contentEquals("+")) {
				suma(n1,n2,operacion,numero,resultado,historial);
					
				}
				else if(ultimoCalculo.contentEquals("-")) {
					resta(n1,n2,operacion,numero,resultado,historial);
				}
				else if(ultimoCalculo.contentEquals("*")) {
					multiplicacion(n1,n2,operacion,numero,resultado,historial);
				}
				else if(ultimoCalculo.contentEquals("/")) {
					division(n1,n2,operacion,numero,resultado,historial);
				}
				else if(ultimoCalculo.contentEquals("%")) {
					porcentaje(n1,n2,operacion,numero,resultado,historial);
				}
			
				
			}
		});
		btnNewButton_1_5.setBounds(278, 420, 94, 73);
		contentPane.add(btnNewButton_1_5);
		
		
		
		setVisible(true);

		
		
	
	}
	
	public static double suma( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 = Double.parseDouble(numero.getText());
		operacion = n2 + n1;
		historial.setText(historial.getText()+ (n1+ " + " + n2 + " = " + operacion + "\n"));
		numero.setText(String.valueOf(operacion));
		resultado.setText(String.valueOf(operacion));
		return operacion;
		
	}
	
	public static double resta( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 = Double.parseDouble(numero.getText());
		operacion = n1 - n2;
		historial.setText(historial.getText()+ (n1+ " - " + n2 + " = " + operacion + "\n"));
		numero.setText(String.valueOf(operacion));
		resultado.setText(String.valueOf(operacion));
		return operacion;
		
	}
	
	public static double multiplicacion( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 = Double.parseDouble(numero.getText());
		operacion = n2 * n1;
		historial.setText(historial.getText()+ (n1+ " * " + n2 + " = " + operacion + "\n"));
		numero.setText(String.valueOf(operacion));
		resultado.setText(String.valueOf(operacion));
		return operacion;
		
	}
	public static double division( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 = Double.parseDouble(numero.getText());
		operacion = n1 / n2;
		historial.setText(historial.getText()+ (n1+ " / " + n2 + " = " + operacion + "\n"));
		numero.setText(String.valueOf(operacion));
		resultado.setText(String.valueOf(operacion));
		return operacion;
		
	}
	public static double porcentaje( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 = Double.parseDouble(numero.getText());
		operacion = (n1/100) * n2;
		historial.setText(historial.getText()+"El "+ n2 + (" de ") + n1 + (" = ") + operacion + "\n");
		numero.setText(String.valueOf(operacion));
		resultado.setText(String.valueOf(operacion));
		return operacion;
		
	}
	
	public static double raizCuadrada( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 =  Math.sqrt(Double.parseDouble(numero.getText()));
		historial.setText(historial.getText()+ numero.getText() + (" √ ")+ (" = ") + n2 + "\n");
		numero.setText(String.valueOf(n2));
		resultado.setText(String.valueOf(n2));
		return n2;
		
	}
	
	public static double potencia( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 =  ((Double.parseDouble(numero.getText()) * (Double.parseDouble(numero.getText()))));
		historial.setText(historial.getText()+numero.getText()+ (" ^2 ") + (" = ") + n2 + "\n");
		numero.setText(String.valueOf(n2));
		resultado.setText(String.valueOf(n2));

		return n2;
	}
	
	
	public static double dividir1( double n1, double n2, double operacion, JLabel numero, JLabel resultado, JTextArea historial) {
		n2 = 1 / Double.parseDouble(numero.getText());
		historial.setText(historial.getText()+" 1 "+ (" / ") + n2 + (" = ") + operacion + "\n");
		numero.setText(String.valueOf(n2));
		resultado.setText(String.valueOf(n2));
		return n2;
	}
	
	
	
	
	
	
	}
	

